# AFS30 - Afstuderen #

Dit repository is bedoeld voor studenten en docenten van de opleiding Elektrotechniek van de Hogeschool Rotterdam en wordt gebruikt om studiemateriaal voor de cursus "AFS30 - Afstuderen" te verspreiden. 

Alle informatie is te vinden op de [Wiki](https://bitbucket.org/HR_ELEKTRO/afs30/wiki/).

